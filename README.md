# Instruções
### Deve conter instalado na máquina
- Docker / Docker Compose
- Composer 
- Symfony
- Angular CLI
- Node / npm


## Api
- Depois de clonar o projeto, dentro da pasta api rodar o comando abaixo para instalar as dependências do projeto

<pre>
    composer install
</pre>
- Iniciar o servidor com o comando
<pre>
    symfony serve -d --no-tls
</pre>
- Verificar se a url do banco no arquivo .env está devidamente configurada com o arquivo compose.yaml
- Dentro da pasta api subir o container do docker com o comando
<pre>
    docker-compose up -d
</pre>

- Com o banco rodando no docker, deve rodar o comando abaixo para executar as migrations
<pre>
    symfony console doctrine:migrations:migrate
</pre>

## Front
- Entrar na pasta front e executar o comando abaixo para instalar as dependencias do projeto
<pre>
    npm install
</pre>
- Para servir o projeto deve rodar o comando
<pre>
    ng serve
</pre>


