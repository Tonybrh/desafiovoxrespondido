# API Documentation
packages
- serializer
- nelmio
- doctrine

## Companies
> Properties
- name
- email
- cnpj

### List All Companies
- **Endpoint**: `GET /company`
- **Description**: Retorna todas as empresas cadastradas.
- **Response**: 
    ```json
    {
      "message": "Array of companies"
    }
    ```

### Create a Company
- **Endpoint**: `POST /company`
- **Description**: Cria uma nova empresa com os dados fornecidos.
- **Body**:
    ```json
    {
      "name": "string",
      "email": "string",
      "cnpj": "string"
    }
    ```
- **Response**:
    ```json
    {
      "message": "Nova empresa inserida com sucesso"
    }
    ```

### Update a Company
- **Endpoint**: `PUT /company/{id}`
- **Description**: Atualiza os dados de uma empresa existente.
- **Body**:
    ```json
    {
      "name": "string",
      "email": "string",
      "cnpj": "string"
    }
    ```
- **Response**:
    ```json
    {
      "message": "Empresa atualizada com sucesso"
    }
    ```

### Delete a Company
- **Endpoint**: `DELETE /company/{id}`
- **Description**: Deleta uma empresa específica.
- **Response**:
    ```json
    {
      "message": "Empresa deletada com sucesso"
    }
    ```

## Partners
> Properties
- name
- email
- cpf

### List All Partners
- **Endpoint**: `GET /partner`
- **Description**: Retorna todos os sócios cadastrados.
- **Response**: 
    ```json
    {
      "message": "Array of partners"
    }
    ```

### Show Partner Details
- **Endpoint**: `GET /partner/show/{id}`
- **Description**: Mostra os detalhes de um sócio específico.
- **Response**: 
    ```json
    {
      "message": "Partner details"
    }
    ```

### Register a Partner
- **Endpoint**: `POST /partner/register`
- **Description**: Registra um novo sócio.
- **Body**:
    ```json
    {
      "name": "string",
      "email": "string",
      "cpf": "string",
      "company": "companyId"
    }
    ```
- **Response**:
    ```json
    {
      "success": true
    }
    ```

### Update Partner Details
- **Endpoint**: `PUT /partner/{id}`
- **Description**: Atualiza os detalhes de um sócio.
- **Body**:
    ```json
    {
      "name": "string",
      "email": "string",
      "cpf": "string",
      "company": "companyId or null"
    }
    ```
- **Response**:
    ```json
    {
      "data": "Sócio atualizado com sucesso"
    }
    ```

### Delete a Partner
- **Endpoint**: `DELETE /partner/{id}`
- **Description**: Deleta um sócio específico.
- **Response**:
    ```json
    {
      "message": "Sócio deletado com sucesso"
    }
    ```
