<?php
namespace App\Serializer;

class CircularReferenceHandler
{
    public static function handleCircularReference($object)
    {
        // Aqui você pode retornar o que quiser quando uma referência circular for encontrada.
        // Por exemplo, você pode retornar o ID do objeto, ou algum outro valor único.
        return $object->getId();
    }
}
?>