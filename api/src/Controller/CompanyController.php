<?php

namespace App\Controller;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use App\Repository\PartnerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends AbstractController
{
    // ! Tem como criar um prefixo global no routes.yml
    #[Route('/company', name: 'company_list', methods: ['GET'])]
    public function index(CompanyRepository $companyRepository): JsonResponse
    {
        $companies = $companyRepository->findAll();

        return $this->json(["message" => $companies]);
    }
    #[Route('/company', name: 'company_create', methods: ['POST'])]
    public function create(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {

        $data = json_decode($request->getContent(), true);
        if ($data === null || !isset($data['name'], $data['email'], $data['cnpj'])) {
            // Retorne uma resposta com um código de status 400 (Bad Request)
            return $this->json(['message' => 'Dados inválidos'], 400);
        }
        $company = new Company();
        $company->setName($data['name']);
        $company->setEmail($data['email']);
        $company->setCnpj($data['cnpj']);

        $entityManager->persist($company);
        $entityManager->flush();

        return $this->json([
            'success' => true,
        ], 200);
    }
    #[Route('/company/{id}', name: 'company_update', methods: ['PUT'])]
    public function update($id, Request $request, CompanyRepository $companyRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $company = $companyRepository->find($id);
        if (!$company) {
            return $this->json(["Message" => "Parceiro não encontrado"], 404);
        }
        if (isset($data['name'])) {
            $company->setName($data['name']);
        }
        if (isset($data['email'])) {
            $company->setEmail($data['email']);
        }
        if (isset($data['cnoj'])) {
            $company->setCnpj($data['cnpj']);
        }
        $entityManager->persist($company);
        $entityManager->flush();
        return $this->json([
            'message' => 'Empresa atualizada com sucesso',
        ]);
    }
    #[Route('/company/{id}', name: 'company_delete', methods: ['DELETE'])]
    public function delete($id, CompanyRepository $companyRepository, PartnerRepository $partnerRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        $company = $companyRepository->find($id);
    
        if (!$company) {
            return $this->json(['message' => 'Empresa não encontrada'], 404);
        }
    
        $partners = $partnerRepository->findBy(['company' => $id]);
    
        foreach ($partners as $partner) {
            $partner->setCompany(null);
            $entityManager->persist($partner);
        }
    
        $entityManager->remove($company);
        $entityManager->flush();
    
        return $this->json([
            'message' => 'Empresa deletada com sucesso',
        ]);
    }
}
