<?php

namespace App\Controller;

use App\Repository\PartnerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\Partner;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;


class PartnerController extends AbstractController
{
    #[Route('/partner', name: 'show_partners', methods: ['GET'])]
    public function index(PartnerRepository $partnerRepository): JsonResponse
    {
        $partners = $partnerRepository->findAll();

        return $this->json(["message" => $partners]);
    }
    #[Route('/partner/show/{id}', name: 'show_partner', methods: ['GET'])]
    public function show($id, PartnerRepository $partnerRepository): JsonResponse
    {
        $partner = $partnerRepository->find($id);

        return $this->json(["message" => $partner]);
    }
    #[Route('/partner/register', name: 'create_partners', methods: ['POST'])]
    public function create(Request $request, PartnerRepository $partnerRepository, CompanyRepository $companyRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        // Verificar se os campos obrigatórios estão presentes
        if ($data === null || !isset($data['name'], $data['email'], $data['cpf'])) {
            return $this->json(['message' => 'data required'], 400);
        }
        if ($data['name'] == "" || $data['email'] == "" || $data['cpf'] == "") {
            return $this->json(['message' => 'data required'], 400);
        }
        // Verificar se o email já está cadastrado
        $existingPartner = $partnerRepository->findOneBy(['email' => $data['email']]);
        if ($existingPartner !== null) {
            // Email já está em uso
            return $this->json(['message' => 'already exists'], 400);
        }

        $partner = new Partner();
        $partner->setName($data['name']);
        $partner->setEmail($data['email']);
        $partner->setCpf($data['cpf']); // Considere usar uma função de hash segura

        // Tratar 'company' como opcional
        $company = $companyRepository->find($data['company']);
        if (!$company) {
            return $this->json(['message' => 'not found'], 404);
        }
        $partner->setCompany($company);

        $entityManager->persist($partner);
        $entityManager->flush();

        return $this->json([
            'success' => true,
        ]);
    }

    #[Route('/partner/{id}', name: 'update_partners', methods: ['PUT'])]
    public function update($id, PartnerRepository $partnerRepository, Request $request, CompanyRepository $companyRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $partner = $partnerRepository->find($id);
        if (!$partner) {
            // Se a entidade não for encontrada, retorne um erro
            return $this->json(['message' => 'Parceiro não encontrado'], 404);
        }
        if (isset($data['name'])) {
            $partner->setName($data['name']);
        }
        if (isset($data['email'])) {
            $partner->setEmail($data['email']);
        }
        if (isset($data['cpf'])) {
            $partner->setCpf($data['cpf']);
        }
        if (isset($data['company']) || array_key_exists('company', $data)) {
            $company = $data['company'] ? $companyRepository->find($data['company']) : null;
            if (!$company && $data['company'] !== null) {
                return $this->json(['message' => 'Empresa não encontrada'], 404);
            }
            $partner->setCompany($company);
        }
        $entityManager->persist($partner);
        $entityManager->flush();
        return $this->json([
            'data' => "Sócio atualizado com sucesso",
        ]);
    }
    #[Route('/partner/{id}', name: 'partner_delete', methods: ['DELETE'])]
    public function delete($id, PartnerRepository $partnerRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        $partner = $partnerRepository->find($id);

        if (!$partner) {
            return $this->json(['message' => 'Empresa não encontrada'], 404);
        }

        $partners = $partnerRepository->findBy(['company' => $id]);

        foreach ($partners as $partner) {
            $partner->setCompany(null);
            $entityManager->persist($partner);
        }

        $entityManager->remove($partner);
        $entityManager->flush();

        return $this->json([
            'message' => 'Sócio deletada com sucesso',
        ]);
    }
}
