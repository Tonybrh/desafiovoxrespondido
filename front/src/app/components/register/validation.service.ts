// validation.service.ts
import { AbstractControl, Validators, ValidatorFn } from '@angular/forms';

export class ValidationService {
  static cpfValidator(control: AbstractControl) {
    return (control: AbstractControl): Validators | null => {
      const cnpj = control.value;

      if (!cnpj) {
        return null; // consider valid if empty, use required validator separately if needed
      }

      const cleanedCnpj = cnpj.replace(/[^\d]+/g, '');

      if (cleanedCnpj.length !== 14) {
        return { cnpjInvalid: true };
      }

      // Place CNPJ validation logic here
      // This is a simplified version, consider using a complete validation logic
      // for production purposes.

      // If CNPJ is invalid
      // return { cnpjInvalid: true };

      // If CNPJ is valid
      return null;
    };
  }
}
