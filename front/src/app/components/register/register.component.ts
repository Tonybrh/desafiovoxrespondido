import { Component, OnInit, inject } from '@angular/core';
import {  Router,RouterLink, RouterLinkActive } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [RouterLink, RouterLinkActive],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit{
  http = inject(HttpClient);
  router = inject(Router)
 // Declare the registerForm property

  ngOnInit(): void {

  }
  async sendRegister(e: Event) {
    e.preventDefault();
    const form = e.target as HTMLFormElement;
    const email = (form.elements.namedItem('userEmail') as HTMLInputElement).value;
    const cpf = (form.elements.namedItem('userCpf') as HTMLInputElement).value;
    const name = (form.elements.namedItem('userName') as HTMLInputElement).value;
    const company = (form.elements.namedItem('companyId') as HTMLInputElement).value;
    const body = {name: name, email: email, cpf: cpf, company: company};
    try {
      this.http.post('http://127.0.0.1:8000/partner/register', body).subscribe({
        next: (data: any) => {
          console.log(data);
          if (data.success) {
            alert("Sócio Cadastrado com sucesso")
          }
          if(data.message == 'not found') {
            console.log('Data message is "not found"');
            alert('Empresa não encontrada');
          }
        },
        error: (error: any) => {
          if(error.error.message === 'not found'){
            alert('Empresa não encontrada, por favor cadastre a empresa');
          }
          console.error(error);
        }
      });
    } catch (error) {
      console.error(error);

    }

  }
}
