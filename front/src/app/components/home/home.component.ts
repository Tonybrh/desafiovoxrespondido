import { Component, OnInit, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgFor, NgIf } from '@angular/common';
import { RouterLink, RouterLinkActive } from '@angular/router';
@Component({
  selector: 'app-home',
  standalone: true,
  imports: [NgIf, NgFor, RouterLink, RouterLinkActive],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  http = inject(HttpClient);
  companies: any[] = [];
  ngOnInit(): void {

  }
  async getCompanies(){
    console.log("opa");

    try {
     this.http.get('http://localhost:8000/company').subscribe((data: any)=>{
      console.log(data.message);
      this.companies = data.message;
    })

  } catch(e){
    console.error(e);

  }
  }
  async sendRegisterCompany(e: Event) {
    e.preventDefault();
    const form = e.target as HTMLFormElement;
    const email = (form.elements.namedItem('email') as HTMLInputElement).value;
    const cnpj = (form.elements.namedItem('cnpj') as HTMLInputElement).value;
    const name = (form.elements.namedItem('name') as HTMLInputElement).value;
    const body = {name: name, email: email, cnpj: cnpj};

    try {
      this.http.post('http://127.0.0.1:8000/company', body).subscribe((data:any) => {
      console.log(data);
      if (data.success) {
        alert("Empresa cadastrada com sucesso!")
      } else {
        alert('[ERROR] Preencha todos os campos!');
      }
    })
    } catch (error) {
      console.error(error);

    }

  }
}
